import java.util.*; 
public class Main {
  public static void main(String[] args) {
   String star ="*";
   int rows  = 7;
   int cols = 0;
   int spaces =0;
   System.out.println("Star(*) Pattern 3");
   for(int r =1; r<=rows; r++){
     spaces = rows - r;
	   for(int s1 =0; s1<spaces; s1++){
       System.out.print(" ");
     }
	   cols = r + (r-1) ;
     for(int c =0; c<cols; c++){
       System.out.print(star);
     }
     System.out.println();
     cols++;
   }
 }
}

/* output

      *
     ***
    *****
   *******
  *********
 ***********
*************

*/