import java.util.*;

class Checker  implements Comparator<Player> {

/***
Given an array of  Player objects, write a comparator that sorts them in order of decreasing score; 
if  or more players have the same score, sort those players alphabetically by name. 
****/ 
 public int compare(Player a, Player b) 
    { 
        int d = b.score - a.score;
        if(d!=0){
            return d;
        }
        else{
             return a.name.compareTo(b.name);
        }
         
    } 
}

class Player{
    String name;
    int score;
    
    Player(String name, int score){
        this.name = name;
        this.score = score;
    }
}

class Solution {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        Player[] player = new Player[n];
        Checker checker = new Checker();
        
        for(int i = 0; i < n; i++){
            player[i] = new Player(scan.next(), scan.nextInt());
        }
        scan.close();
     
        Arrays.sort(player, checker);
        for(int i = 0; i < player.length; i++){
            System.out.printf("%s %s\n", player[i].name, player[i].score);
        }
    }
}