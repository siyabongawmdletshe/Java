import java.util.*; 
public class Main {
  public static void main(String[] args) {
   String star ="*";
   int rows  = 8;
   int cols = 1;
   int cols2 = 9;
   int cols3 = 8;
   System.out.println("Star(*) Pattern 8");
    System.out.println("");
   for(int r =1; r<=rows; r++){
     for(int c =0; c<cols; c++){
       System.out.print(star);
     }
	 
	 for(int c2 =0; c2<rows-r; c2++){
       System.out.print(" ");
     }
     
     for(int c3 =0; c3<cols2; c3++){
       System.out.print(star);
     }
  
     for(int c5 =1; c5<=r; c5++){
         if(c5!=1){System.out.print(" ");System.out.print(" ");}
       
     }
     for(int c4 =0; c4<cols3; c4++){
       System.out.print(star);
     }
     
     for(int c7 =0; c7<rows-r; c7++){
         System.out.print(" ");
     }
      for(int c8 =1; c8<=r; c8++){
       System.out.print(star);
     }
     
     cols3--;
     cols2--;
     System.out.println();
     cols++;
   }
}
}
/* output

*       *****************       *
**      ********  *******      **
***     *******    ******     ***
****    ******      *****    ****
*****   *****        ****   *****
******  ****          ***  ******
******* ***            ** *******
**********              *********

*/