import java.util.*; 
public class Main {
  public static void main(String[] args) {
   String star ="*";
   int rows  = 7;
   int cols = 7;
   System.out.println("Star(*) Pattern 7");
   for(int r =0; r<rows; r++){
     for(int c =0; c<cols; c++){
       if(r==0 || r==6){
          System.out.print(star);
       }else{
         if(c==0 || c==6){
         System.out.print(star);
       }
       else{System.out.print(" ");}
       }
       
       
     }
     System.out.println();
   }
}
}
/* output

*******
*     *
*     *
*     *
*     *
*     *
*******


*/