package dice;

import java.util.ArrayList;
import java.util.Scanner;

public class Centennial extends Dice {
    private int size;
	@Override
	protected ArrayList<Integer> rollDice() {
		ArrayList<Integer> rolledDices = new ArrayList<Integer>();
		rolledDices.add((int)(1+Math.random()*6));
		rolledDices.add((int)(1+Math.random()*6));
		rolledDices.add((int)(1+Math.random()*6));
		return rolledDices;
	}
	@Override
	protected void play(int numberOfPlayers) {
		
		size = numberOfPlayers;
		int turn = 0;
		String [][] board = new String[size][12]; 
	    int [] moveBack = new int [size];
	    for(int i=0;i<size; i++){
	    	moveBack[i]=12;
	    }
		for(int player = 0; player<size; player++){
			for(int cell =0; cell<12; cell++){
				board[player][cell] ="";
			}
		}
		
		do{
			System.out.println("####################################");
			ArrayList<Integer> dice = rollDice();
			int sum = dice.get(0)+ dice.get(1) + dice.get(2);
			if(sum<=12){
				board[turn][sum-1] =sum+"";
				for(int cell=0; cell<12; cell++){
					if(cell==sum-1)
						System.out.print("["+sum+"->] ");
					else
						System.out.print("[] ");
				}
			}
			else{
				moveBack[turn] -=(sum-12);
			    if(moveBack[turn]<=1){
			    	System.out.println("[1] ");
			    	System.out.println("Player "+(turn+1)+" is a winner!");
			    	System.out.println("");
					System.out.println("####################################");
			    	break;
			    }
				board[turn][moveBack[turn]] =moveBack[turn]+"";
				for(int cell =0; cell<12; cell++){
					if(cell==moveBack[turn]-1)
						System.out.print("[<-"+moveBack[turn]+"] ");
					else
						System.out.print("[] ");
				}
			}
			System.out.println("");
			System.out.println("####################################");
			System.out.println("");
				turn++;
				if(turn==size)
					turn =0;
		}
		while(true);
	}
}