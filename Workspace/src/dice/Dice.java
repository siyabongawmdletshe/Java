package dice;

import java.util.ArrayList;

public abstract class Dice {
	private ArrayList<Integer> dices = new ArrayList<Integer>();
	public Dice(){
		initializeDices();
	}
	private void initializeDices(){
		for(int i=1; i<7;i++ ){
			dices.add(i);
		}
	}
	protected abstract ArrayList<Integer> rollDice();
	protected abstract void play(int numberOfPlayers);

}
