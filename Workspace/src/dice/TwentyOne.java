package dice;

import java.util.ArrayList;
import java.util.Scanner;

public class TwentyOne extends Dice {
    private int [] playersTotal;
    private int tie = 0;
    private int size;
	private void getWinner() {
		int winner = playersTotal[0];
		int winnerIndex =0;
		for(int i=1; i<size; i++)
			if(playersTotal[i]>winner){
				winner = playersTotal[i];
				winnerIndex = i;
			}
			else if(playersTotal[i]==winner)
				tie++;
		
		if(tie>0){
			System.out.print("There's a tie between the following Players [ ");
			for(int i=0; i<size; i++){
				if(playersTotal[i]==winner){
					System.out.print(i+" ");
					winnerIndex = i;
				}
			}
			System.out.print("] with a total of "+playersTotal[winnerIndex]+".");
		return;
		}
		System.out.println("#########################################################################");
		System.out.println("The winner is Player: "+(winnerIndex+1) +" with a total of "+ playersTotal[winnerIndex]);
		System.out.println("#########################################################################");
	}
	@Override
	protected ArrayList<Integer> rollDice() {
		ArrayList<Integer> rolledDices = new ArrayList<Integer>();
		rolledDices.add((int)(1+Math.random()*6));
		return rolledDices;
	}
	@Override
	protected void play(int numberOfPlayers) {
		playersTotal = new int [numberOfPlayers];
		size = numberOfPlayers;
		int turn = 1;
		Scanner scanner = new Scanner(System.in);
		do{
		   System.out.println("Player "+(turn)+", Do you want to stand on your current score, or continue throwing?\nPress Y to continue or N to stand");	
		   String choice = scanner.next();
		   if(choice.toLowerCase().equals("y")){
			   int rolledDice = rollDice().get(0);
			   playersTotal[turn-1] += rolledDice;
			   System.out.println("You have rolled "+rolledDice+" and your total score is "+playersTotal[turn-1]);
			   if(playersTotal[turn-1]>21){
				   System.out.println("OOPS! Your total is over 21. You are out of the game.");
				   playersTotal[turn-1]=0;
				   if(numberOfPlayers==2){
					   System.out.println("#########################################################################");
					   System.out.println("Player "+numberOfPlayers+" is a WINNER!");
					   System.out.println("#########################################################################");
					   break;
				   }
				   numberOfPlayers--;
				   turn++;
			   }
			   System.out.println();
		   }
		   else{
			   if(turn==size){
				   getWinner(); break;
				}
			   System.out.println("Your total score is "+playersTotal[turn-1]+". Goodluck...");
			   System.out.println();
			   turn++;
		   }
		   
		}
		while(true);
	}
}