/**
 * 
 */
package Calender;
import java.util.Scanner;
/**
 * @author siyabongawmdletshe
 *
 */
public class Main {

	/**
	 * @param args
	 */
    
	private static Scanner reader = new Scanner(System.in);
	private static int firstDayInAmonth=0;
	private static int leapDays=29;
	private static void ThirtyDays(int month, int rows, int firstDay){	
		switch (month) {
		case 4: System.out.println("April"+"\n"+"---------------------------------------------------\n");
			break;
		case 6: System.out.println("June"+"\n"+"---------------------------------------------------\n");
		   break;
		case 9: System.out.println("Septermber"+"\n"+"---------------------------------------------------\n");
		   break;
		case 11: System.out.println("November"+"\n"+"---------------------------------------------------\n");
		   break;
		default:
			break;
		}
		
		System.out.println("Sun\tMon\tTue\tWed\tThu\tFri\tSat");
		boolean found =false;
		int count =0;

		for(int r=0; r<rows; r++){
			for(int c=0; c<7; c++){
				if(r==0){
					if(c!=firstDay){
						if(found){
							System.out.print(++count+" \t");
						}
						else{
							System.out.print("\t");
						}
						
					}
					else{
						System.out.print(++count+" \t");
						found=true;
					}
				}
				else{
					count++;
					if(count<=30){
						System.out.print(count+"\t");
						if(count==30){
							firstDayInAmonth = c+1;
						}
						
					}
					
				}
				
			}
			System.out.println();
			
		}
		System.out.println("");
	}
    private static void ThirtyOneDays(int month, int rows, int firstDay){	
		switch (month) {
		case 1: System.out.println("January"+"\n"+"---------------------------------------------------\n");
			break;
		case 3: System.out.println("March"+"\n"+"---------------------------------------------------\n");
		   break;
		case 5: System.out.println("May"+"\n"+"---------------------------------------------------\n");
		   break;
		case 7: System.out.println("July"+"\n"+"---------------------------------------------------\n");
		   break;
		case 8: System.out.println("Aug"+"\n"+"---------------------------------------------------\n");
		   break;
		case 10: System.out.println("Oct"+"\n"+"---------------------------------------------------n");
		   break;
		case 12: System.out.println("Dec"+"\n"+"---------------------------------------------------\n");
		   break;
		default:
			break;
		}
		
		System.out.println("Sun\tMon\tTue\tWed\tThu\tFri\tSat");
		boolean found =false;
		int count =0;

		for(int r=0; r<rows; r++){
			for(int c=0; c<7; c++){
				if(r==0){
					if(c!=firstDay){
						if(found){
							count++;
							System.out.print(count+" \t");
						}
						else{
							System.out.print("\t");
						}
						
					}
					else{
						count++;
						System.out.print(count+" \t");
						found=true;
					}
				}
				else{
					count++;
					if(count<=31){
						
						System.out.print(count+"\t");
						if(count==31){
							firstDayInAmonth = c+1;
						}
					}
					
				}
				
			}
			System.out.println();
			
		}
		System.out.println("");
	}
    private static void Feb(int month, int rows, int firstDay){
		System.out.println("February"+"\n"+"---------------------------------------------------\n");
		System.out.println("Sun\tMon\tTue\tWed\tThu\tFri\tSat");
		boolean found =false;
		int count =0;
		for(int r=0; r<rows; r++){
			for(int c=0; c<7; c++){
				if(r==0){
					if(c!=firstDay){
						if(found){
							System.out.print(++count+" \t");
						}
						else{
							System.out.print("\t");
						}
					}
					else{
						System.out.print(++count+" \t");
						found=true;
					}
				}
				else{
					count++;
					if(count<=leapDays){
						System.out.print(count+"\t");
						if(count==leapDays){
							firstDayInAmonth = c+1;
						}
					}
					
				}
				
			}
			System.out.println();
			
		}
		System.out.println("");
	}
    private static void displayCalender(){
		int rows=0;
		for(int month=1; month<=12; month++){
			if(firstDayInAmonth<=4){
				rows=5;
			}
			else{
				rows=6;
			}
			if((month%2!=0 && month<=7) || (month%2==0 && month>=8)){
				ThirtyOneDays(month, rows, firstDayInAmonth);
			}
			else if(month==2){
				Feb(month, rows, firstDayInAmonth);
			}
			else{
				ThirtyDays(month, rows, firstDayInAmonth);
			}
		}
	}
    public static void main(String[] args) {
		System.out.println("Year?");
		int year =reader.nextInt();
		System.out.println("First day of the month?");
		firstDayInAmonth =reader.nextInt();
		
		if((year%100==0 && year%400!=0) || year%4!=0){
			leapDays=28;
		}
		displayCalender();
	}
}