/**
 * 
 */
package scissorRockPaper;
import java.util.Scanner;
/**
 * @author siyabongawmdletshe
 *
 */
public class ScissorRockPaper {

	/**
	 * @param args
	 */
	
	public static String getString(int input){
		String output="";
		switch (input) {
		case 0:output="scissor";break;
		case 1:output="rock";break;
		case 2:output="paper";break;
		default:
			break;
		}
		return output;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner reader = new Scanner(System.in);
		
		try {
			int userInput=0;
			
			do{
				do{
					int computer = (int)(Math.random()*3);
					System.out.print("scissor (0), rock (1), paper (2): ");
					userInput = reader.nextInt();
				    if(computer==0 && userInput==2){
						System.out.println("The computer is "+getString(computer)+". You are "+getString(userInput)+". You lose");
					}
					else if(computer==0 && userInput==1){
						System.out.println("The computer is "+getString(computer)+". You are "+getString(userInput)+". You win");
					}
					else if(computer==1 && userInput==2){
						System.out.println("The computer is "+getString(computer)+". You are "+getString(userInput)+". You win");
					}
					else if(computer==1 && userInput==0){
						System.out.println("The computer is "+getString(computer)+". You are "+getString(userInput)+". You lose");
					}
					else if(computer==2 && userInput==0){
						System.out.println("The computer is "+getString(computer)+". You are "+getString(userInput)+". You win");
					}
					else if(computer==2 && userInput==1){
						System.out.println("The computer is "+getString(computer)+". You are "+getString(userInput)+". You lose");
					}
					else{
						System.out.println("The computer is "+getString(computer)+". You are "+getString(userInput)+" too. It is a draw");
					}
				}
				while(userInput==0);
				    System.out.println("");
				    System.out.println("");
			}
			while(true);
			
		} catch (Exception e) {
			System.out.print(e);
		}
	}

}
