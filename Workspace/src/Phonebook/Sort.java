/**
 * 
 */
package Phonebook;
import java.util.Scanner;
/**
 * @author siyabongawmdletshe
 *
 */
public class Sort {

	/**
	 * @param args
	 */
	
	
	public static void ascending(int [] array, int size){
		
		for(int i=0;i<size; i++){
			int minIndex=i;
			for(int j=i+1;j<size; j++){
				if(array[minIndex]>array[j]){
					int temp=array[minIndex];
					array[minIndex]=array[j];
					array[j]=temp;
				}
			}
		}
		System.out.println("Ascending order ");
		for(int i=0;i<size; i++){
			System.out.print(array[i]+" ");
		}
	}
	public static void descending(int [] array, int size){
		for(int i=0;i<size; i++){
			int maxIndex=i;
			for(int j=i+1;j<size; j++){
				if(array[maxIndex]<array[j]){
					int temp=array[maxIndex];
					array[maxIndex]=array[j];
					array[j]=temp;
				}
			}
		}
		System.out.println("Descending order ");
		for(int i=0;i<size; i++){
			System.out.print(array[i]+" ");
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try {
			int [] array =new int[20];
			int size=array.length;
			for(int i=0; i<size; i++){
				array[i]=(int)(Math.random()*101);
			}
			System.out.println("Before anything.. ");
			for(int i=0;i<size; i++){
				System.out.print(array[i]+" ");
			}
			System.out.println("");
			ascending(array, size);
			System.out.println();
			descending(array, size);
			array = null;
		} catch (Exception e) {
			System.out.print(e);
		}
	}

}
