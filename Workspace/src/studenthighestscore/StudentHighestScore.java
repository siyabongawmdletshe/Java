/**
 * 
 */
package studenthighestscore;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * @author siyabongawmdletshe
 *
 */
public class StudentHighestScore {

	/**
	 * @param args
	 */
	
	public static void printMarkList(List<Double> list){
		int size = list.size();
		for(int i=0; i<size; i++){
			System.out.print(list.get(i)+" ");
		}
	}
	public static Double getLowest(List<Double> list){
		Collections.sort(list);
		return list.get(0);
	}
	public static Double getHighest(List<Double> list){
		Collections.sort(list);
		return list.get(list.size()-1);
	}
	public static Double getAverage(List<Double> list){
		int size = list.size();
		double sum=0;
		for(int i=0; i<size; i++){
			sum+=list.get(i);
		}
		return sum/size;
	}
	public static Integer failedStudents(List<Double> list){
		int size = list.size();
		int failed=0;
		for(int i=0; i<size; i++){
			if(list.get(i)<50){
				failed++;
			}
		}
		return failed;
	}
	public static Integer passedStudents(List<Double> list){
		int size = list.size();
		int passed=0;
		for(int i=0; i<size; i++){
			if(list.get(i)>=50){
				passed++;
			}
		}
		return passed;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner reader = new Scanner(System.in);
		
		try {
			int numberOfStudents=0;
			
			do{
				do{
					
					System.out.print("Number of students: ");
					numberOfStudents = reader.nextInt();
					List<Double> studentMarks = new ArrayList<Double>();
					for(int i=0; i<numberOfStudents; i++){
						double marks = (int)(Math.random()*101);
						studentMarks.add(marks);
					}
					if(numberOfStudents>0){
						System.out.print("Student Marks: ");
						printMarkList(studentMarks);
						System.out.println("");
						System.out.println("Lowest mark: "+ getLowest(studentMarks));
						System.out.println("Highest mark: "+getHighest(studentMarks));
						System.out.println("Average mark: "+getAverage(studentMarks));
						System.out.println("Failed students: "+failedStudents(studentMarks));
						System.out.println("Passed students: "+passedStudents(studentMarks));
					}
					
					
				}
				while(numberOfStudents==0);
				    System.out.println("");
				    System.out.println("");
			}
			while(true);
			
		} catch (Exception e) {
			System.out.print(e);
		}
	}

}
