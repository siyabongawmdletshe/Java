/**
 * 
 */
package chess;
import javax.swing.JFrame;

/**
 * @author siyabongawmdletshe
 *
 */
public class Main {

	/**
	 * @param args
	 */
	
	public static void main(String[] args) {
		 ChessBoard chessBoard = new ChessBoard(8);
		 chessBoard.setSize(800, 800); 
		 chessBoard.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 chessBoard.setResizable(false);
		 chessBoard.setVisible(true);
	}

}
