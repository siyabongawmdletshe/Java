package chess;
import java.awt.Color;
import java.awt.GridLayout;
import java.net.URL;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
public class ChessBoard extends JFrame {
	/**
	 * 
	 */
	private int _boardSize;
	private JPanel main = new JPanel();
    public ChessBoard(int boardSize) {
    	_boardSize = boardSize;
    	this.add(main);
    	drawBoard();
    	
	}
    private URL getCellIcon(int row, int column, int boardSize){
    	URL icon = null;
    	//first and last rows
    	if(row==0){
    		switch (column){
    		case 0: icon =getClass().getResource("cb.gif"); break;
    		case 1: icon =getClass().getResource("knb.gif"); break;
    		case 2: icon =getClass().getResource("bb.gif"); break;
    		case 3: icon =getClass().getResource("qb.gif"); break;
    		case 4: icon =getClass().getResource("kb.gif"); break;
    		case 5: icon =getClass().getResource("bb.gif"); break;
    		case 6: icon =getClass().getResource("knb.gif"); break;
    		case 7: icon =getClass().getResource("cb.gif"); break;
    		default: break;
    		}
    	}
        else if(row==1){
        	 icon =getClass().getResource("pb.gif");
    	}
    	else if(row==boardSize-2){
    		 icon =getClass().getResource("pw.gif");
    	}
        else if(row==boardSize-1){
        	switch (column){
    		case 0: icon =getClass().getResource("cw.gif"); break;
    		case 1: icon =getClass().getResource("knw.gif"); break;
    		case 2: icon =getClass().getResource("bw.gif"); break;
    		case 3: icon =getClass().getResource("qw.gif"); break;
    		case 4: icon =getClass().getResource("kw.gif"); break;
    		case 5: icon =getClass().getResource("bw.gif"); break;
    		case 6: icon =getClass().getResource("knw.gif"); break;
    		case 7: icon =getClass().getResource("cw.gif"); break;
    		default: break;
    		}
    	}
    	
       return icon;	
    }
    
    private Color cellColor(int row, int column, int boardSize){
    	Color ccolor =new Color(255,255,255);
    	if(row%2==0){
    		if(column%2!=0){
    			ccolor = new Color(51,204,255);
    		}
    	}
    	else{
    		if(column%2==0){
    			ccolor = new Color(51,204,255);
    		}
    	}
		return ccolor ;
    }
    private void drawBoard(){
    	this.setTitle("Chess Board");
    	main.setLayout(new GridLayout(_boardSize,_boardSize,0,0));
    	for(int i=0; i<_boardSize; i++){
    		for(int j=0; j<_boardSize; j++){
    			JLabel label = new JLabel();
    			if(i==0 || i==1 || i==_boardSize-2 || i==_boardSize-1){
    				ImageIcon image = new ImageIcon(getCellIcon(i,j,_boardSize));
    				label = new JLabel(image);
    			}
    			JPanel cell = new JPanel();
    			cell.setLayout(new GridLayout(1,0));
    			cell.add(label);
    			cell.setBackground(cellColor(i, j, _boardSize));
        		main.add(cell);
        	}
    	}
    }
} 