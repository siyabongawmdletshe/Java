/**
 * 
 */
package pyramid;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * @author siyabongawmdletshe
 *
 */
public class DisplayingPyramid {

	/**
	 * @param args
	 */
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner reader = new Scanner(System.in);
		
		try {
			int pyramidLimit=0;
			
			do{
				do{
					System.out.print("Input must be between 1 and 10: ");
					pyramidLimit = reader.nextInt();
					if(pyramidLimit>0 && pyramidLimit<=10){
						int dec =0;
						for(int row=0; row<pyramidLimit; row++){
							dec =row+1;
							for(int spaces=0; spaces<pyramidLimit-(row+1); spaces++){
								System.out.print(" ");
							}
							for(int j=0; j<row+row+1; j++){
								
								System.out.print(dec);
								if(j>row-1){
									
									dec++;
								}else{
									dec--;
								}
							}
							for(int spaces=0; spaces<pyramidLimit-(row+1); spaces++){
								System.out.print(" ");
							}
							System.out.println();
						}
						
						System.out.println();
						System.out.println("pyramid 2");
						int dec1 =2;
						for(int row=0; row<pyramidLimit; row++){
							
							for(int spaces=0; spaces<pyramidLimit-(row+1); spaces++){
								System.out.print(" ");
							}
							for(int j=0; j<row+row+1; j++){
								if(j>row){
									System.out.print(1*(int)Math.pow(2, j-dec1));
									dec1+=2;
								}
								else{
									System.out.print(1*(int)Math.pow(2, j));
								}
									
								
							}
							for(int spaces=0; spaces<pyramidLimit-(row+1); spaces++){
								System.out.print(" ");
							}
							System.out.println();
							dec1=2;
						}
					}
				}
				while(pyramidLimit<=0 || pyramidLimit>10);
				    System.out.println("");
				    System.out.println("");
			}
			while(true);
			
		} catch (Exception e) {
			System.out.print(e);
		}
	}

}
