package lottonumbers;

public class JackpotNumbers {
	private int type;
	private int ntimes;
	private int [][] listOfNumber;
	private static JackpotNumbers jpob=null;
	private JackpotNumbers(){
	}
	public static JackpotNumbers getJackpotNumbers(){
		if(jpob==null)
			jpob= new JackpotNumbers();
		return jpob;
	}
	public void generateLuckyNumbers(int type, int ntimes){
		if(type<1 || type >3)
			return;
		this.ntimes =ntimes;
		this.type=type;
		if(type==1)
			lottoNumbers();
		else if(type==2)
			powerBallNumbers();
		else
			dailyNumbers();
	}
	private void dailyNumbers() {
		listOfNumber = new int [ntimes][5];//5 = 1 - 36
		for(int i=0; i < ntimes; i++)
			for (int j =0; j < 5; j++)
				listOfNumber[i][j] = (int)(1+ Math.random()*35);
	}
	private void powerBallNumbers() {
		// 5 + 1 powerball = 1-50, 1-20
		listOfNumber = new int [ntimes][6];
		for(int i=0; i < ntimes; i++)
			for (int j =0; j < 6; j++)
				listOfNumber[i][j] = j==5?(int)(1+ Math.random()*19):(int)(1+ Math.random()*49);
	}
	private void lottoNumbers() {
		// 6 = 1 - 52
		listOfNumber = new int [ntimes][6];
		for(int i=0; i < ntimes; i++)
			for (int j =0; j < 6; j++)
				listOfNumber[i][j] = (int)(1+ Math.random()*51);
	}
	public String toString(){
		if(listOfNumber==null)
			return null;
		String outputString =type==1?"Lotto Numbers":type==2?"Powerball Numbers":"Daily numbers";
		outputString +="\n{";
		int row = listOfNumber.length;
		for (int i = 0; i < row; i++){
			outputString+="[";
			int colSize= listOfNumber[i].length;
		    for (int j = 0; j < colSize; j++)
		    	outputString+=j<colSize-1?listOfNumber[i][j]+",":listOfNumber[i][j];
		    outputString+=(row>1&&i<row-1)?"], ":"]}";
		 }  
	   return outputString+"\n";
	}
}