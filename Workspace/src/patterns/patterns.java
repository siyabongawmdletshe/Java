/**
 * 
 */
package patterns;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * @author siyabongawmdletshe
 *
 */
public class patterns {

	/**
	 * @param args
	 */
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	Scanner reader = new Scanner(System.in);
		
		try {
			int pyramidLimit=0;
			
			do{
				do{
					System.out.print("Input must be between 1 and 10: ");
					pyramidLimit = reader.nextInt();
					if(pyramidLimit>0 && pyramidLimit<=10){
						System.out.println("+++++++++++++++++");
						System.out.println("pattern 1");
						for(int row=0; row<pyramidLimit; row++){
							for(int j=0; j<=row; j++){
								System.out.print(j+1);
							}
							for(int spaces=0; spaces<pyramidLimit-(row+1); spaces++){
								System.out.print(" ");
							}
							System.out.println();
						}
						
						//pattern 2
						System.out.println();
						System.out.println("+++++++++++++++++");
						System.out.println("pattern 2");
						System.out.println("");
						for(int row=0; row<pyramidLimit; row++){
							for(int spaces=0; spaces<pyramidLimit-(row+1); spaces++){
								System.out.print(" ");
							}
							for(int j=0; j<=row; j++){
								System.out.print(j+1);
							}
							
							System.out.println();
						}
						//pattern 3
						System.out.println();
						System.out.println("+++++++++++++++++");
						System.out.println("pattern 3");
						System.out.println("");
						for(int row=pyramidLimit; row>=0; row--){
							for(int spaces=0; spaces<pyramidLimit-row; spaces++){
								System.out.print(" ");
							}
							for(int j=0; j<=row; j++){
								System.out.print(j+1);
							}
							
							System.out.println();
						}
						//pattern 4
						System.out.println();
						System.out.println("+++++++++++++++++");
						System.out.println("pattern 4");
						System.out.println("");
						for(int row=pyramidLimit; row>=0; row--){
							for(int j=0; j<=row; j++){
								System.out.print(j+1);
							}
							for(int spaces=0; spaces<pyramidLimit-row; spaces++){
								System.out.print(" ");
							}
							System.out.println();
						}
					}
				}
				while(pyramidLimit<=0 || pyramidLimit>10);
				    System.out.println("");
				    System.out.println("");
			}
			while(true);
			
		} catch (Exception e) {
			System.out.print(e);
		}
	}

}
