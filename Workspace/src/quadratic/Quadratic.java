/**
 * 
 */
package quadratic;
import java.util.Scanner;
/**
 * @author siyabongawmdletshe
 *
 */
public class Quadratic {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner reader = new Scanner(System.in);
		
		try {
			double a=0,b=0,c=0;
			
			do{
				do{
					System.out.print("Please enter a, b, c: ");
					a = reader.nextInt();
					b = reader.nextInt();
					c = reader.nextInt();
					
					double discriminant = b*b - 4*a*c;
					double results1 = 0;
					double results2 = 0;
					if(discriminant<0){
						System.out.print("The equation has no real roots");
					}
					else if(discriminant==0){
						results1 = ((-1 * b)+ Math.pow(discriminant, 0.5))/2*a;
						System.out.print("The root is "+results1);
					}
					else{
						results1 = ((-1 * b)+ Math.pow(discriminant, 0.5))/2*a;
						results2 = ((-1 * b)- Math.pow(discriminant, 0.5))/2*a;
						System.out.print("The roots are "+results1+" and "+results2);
					}
				}
				while(a==0 || b==0 || c==0);
				  
					
				    System.out.println("");
					System.out.println("");
			}
			while(true);
			
		} catch (Exception e) {
			System.out.print(e);
		}
		
		
		
		

	}

}
