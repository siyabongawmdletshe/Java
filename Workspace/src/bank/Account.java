package bank;
import java.util.*;

public class Account {
private int idNumber;
private double balance;
Date date;

public Account(int id, double bal, Date dat) {
	idNumber =id;
	balance = bal;
	date=dat;
}
public int getID(){
	return idNumber;
}
public double getBalance(){
	return balance;
}
public Date getDate(){
	return date;
}
public void setID(int id){
	idNumber =id;
}
public void deposit(double bal){
	balance+=bal;
}
public void withdraw(double amount){
	balance-=amount;
}
public String toString(){
	return  "ID number: "+idNumber+" Date: "+date+" Available balance: R"+getBalance() ;
}
public String printBalance(){
	return  "Available balance: R"+getBalance() ;
}
public String printDate(){
	return  "Date of creation: "+date ;
}
} 