/**
 * 
 */
package bank;
import java.util.Date;
import java.util.Scanner;
/**
 * @author siyabongawmdletshe
 *
 */
public class Main {

	/**
	 * @param args
	 */
	private static Account [] accounts = null;
	private static Scanner reader = new Scanner(System.in);
	public static void Operation(int id, int option){
		switch (option) {
		case 1: System.out.print(accounts[id].printBalance());System.out.println();break;
		case 2: System.out.print("How much do you want to withdraw? ");
		        int amount = reader.nextInt();
		        double balance = accounts[id].getBalance();
		        if(amount>0){
		        if(amount<=balance){
		        	accounts[id].withdraw(amount);
			        System.out.println("Amount withdrawn successfully!");
		        }
		        else{
		        	System.out.println("Sorry! Amount is over the balance limit!");
		        }
		        }
		        else{
		        	System.out.println("Sorry! Amount is not valid!");
		        }
		        break;
		case 3: System.out.print("How much do you want to deposit? ");
		        int damount = reader.nextInt();
		        if(damount>0){
		        	accounts[id].deposit(damount);
			        System.out.println("Amount deposited successfully!");
		        }
		        else{
		        	System.out.println("Sorry! Amount is not valid!");
		        }
		        break;
		default:
			break;
		}
	}
	public static void main(String[] args) {
		System.out.print("How many people in a queue? ");
		int queue =reader.nextInt();
		 accounts = new Account[queue];
		for(int i=0; i<queue; i++){
			accounts[i] = new Account(i, 1000, new Date());
		}
		
		int client =0;
		while(queue!=0){
			int option=0;
			System.out.println();
			System.out.println("Welcome to Gougem Bank client "+client);
			do{
				System.out.println("Main menu\n1.  Check Balance\n2.  Withdraw\n3.  Deposit\n4.  Exit");
				option=reader.nextInt();
				if(option>0 && option<4){
					Operation(client,option);
					System.out.println();
				}
			}
			while(option!=0 && option!=4);
			System.out.println("Good bye client "+client+".See you next time.");
			System.out.println();
			queue--;
			client++;
		}
	}

}
