package bank;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class GUI extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel main = new JPanel();
	private JPanel buttons = new JPanel();
	private static JTextArea textArea = new JTextArea();
	private static JTextArea clientArea = new JTextArea();
	private static int count = 0;
	private static Account [] accounts = null;
    private GUI() {
    	this.setTitle("Gougem Bank");
    	addButtonElement();
		addMainElement();
	}
    private void addMainElement(){
    	main.setBackground(Color.BLACK);
    	this.add(main);
    	
    }
    private void addButtonElement(){
    	JPanel textAndButtons = new JPanel();
    	textAndButtons.setLayout(new GridLayout(4,0,0,0));
    	textAndButtons.setBackground(Color.BLACK);
    	JLabel l1 = new JLabel("1. Check Balance");
    	JLabel l2 = new JLabel("2. Withdraw");
    	JLabel l3 = new JLabel("3. Deposit");
    	JLabel l4 = new JLabel("4. Exit");
    	JButton button1 = new JButton(l1.getText());
    	button1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Operation(count,1);
			}
		});
    	JButton button2 = new JButton(l2.getText());
    	button2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Operation(count,2);
			}
		});
    	JButton button3 = new JButton(l3.getText());
    	button3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Operation(count,3);
			}
		});
    	JButton button4 = new JButton(l4.getText());
    	button4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Operation(count,4);
			}
		});
    	textAndButtons.add(button1);
    	textAndButtons.add(button2);
    	textAndButtons.add(button3);
    	textAndButtons.add(button4);
    	
    	clientArea.setBackground(Color.BLACK);
    	clientArea.setForeground(Color.RED);
    	clientArea.setEditable(false);
    	textArea.setBackground(Color.BLACK);
    	textArea.setForeground(Color.RED);
    	textArea.setEditable(false);
    	updateClient();
    	buttons.setLayout(new GridLayout(2,0,0,0));
    	buttons.add(clientArea);
    	buttons.add(textAndButtons);
    	main.add(buttons);
    }
    
    
    private static void updateClient(){
    	clientArea.setText("Welcome to Gougem Bank\n                Client "+count);
    }
    public static void Operation(int id, int option){
		switch (option) {
		case 1: JOptionPane.showMessageDialog(null, accounts[id].printBalance());break;
		case 2: 
		        int amount =Integer.parseInt(JOptionPane.showInputDialog("How much do you want to withdraw?"));
		        double balance = accounts[id].getBalance();
		        if(amount>0){
		        if(amount<=balance){
		        	accounts[id].withdraw(amount);
			        //textArea.setText("Amount withdrawn successfully!");
			        JOptionPane.showMessageDialog(null, "Amount withdrawn successfully!");
		        }
		        else{
		        	//textArea.setText("Sorry! Amount is over the balance limit!");
		        	JOptionPane.showMessageDialog(null, "Sorry! Amount is over the balance limit!");
		        }
		        }
		        else{
		        	//textArea.setText("Sorry! Amount is not valid!");
		        	JOptionPane.showMessageDialog(null, "Sorry! Amount is not valid!");
		        }
		        break;
		case 3: 
		         int damount =Integer.parseInt(JOptionPane.showInputDialog("How much do you want to deposit? "));
		        if(damount>0){
		        	accounts[id].deposit(damount);
			        //textArea.setText("Amount deposited successfully!");
			        JOptionPane.showMessageDialog(null, "Amount deposited successfully!");
		        }
		        else{
		        	//textArea.setText("Sorry! Amount is not valid!");
		        	JOptionPane.showMessageDialog(null, "Sorry! Amount is not valid!");
		        }
		        break;
		case 4: JOptionPane.showMessageDialog(null, "Good bye client "+count+".\nSee you next time.");count++;  
		      if(count<accounts.length){
		    	  updateClient(); textArea.setText("");
		      }
		      else{
		    	  JOptionPane.showMessageDialog(null, "No more people in the Q.");
		    	  setQ();
		      }
	          break;
		default:
			break;
		}
	}
    public static void setQ(){
    	int queue =Integer.parseInt(JOptionPane.showInputDialog("How many people in a queue?"));
		 accounts = new Account[queue];
		for(int i=0; i<queue; i++){
			accounts[i] = new Account(i, 1000, new Date());
		}
    }
	public static void main(String[] args) {
		
		 setQ();
		 GUI frame = new GUI(); // Create a frame
		 frame.setSize(800, 500); // Set the frame size
		 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 frame.setResizable(false);
		 frame.setVisible(true); // Display the frame
		 
	}
} 