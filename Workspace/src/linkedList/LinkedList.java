package linkedList;

/**Linked list implementation developed by Siyabonga Mdletshe**/

public class LinkedList<E> {


	private Node head;
	private Node tail;
	private static LinkedList<Object> list ;
	private LinkedList(){}
	
	
	//Adds item in the last position
	public void addLast(Object data){
	    Node newNode = new Node(data);
	    newNode.nextItem =null;
	    if(head==null){
	    	head = newNode;
	    }
	    else{
	    	tail = head;
	    	while(tail.nextItem!=null){
	    		tail = tail.nextItem;
	    	}
	    	tail.nextItem = newNode;
	    }
	}
	//Adds item in the first position
	public void addFirst(Object data){
	    if(head==null){
	    	head =new Node(data);
	    }
	    else{
	    	Node lastNode = list.head;
	    	head = new Node(data);
	    	head.nextItem = lastNode;
	    }
	}
	
	//Adds item in the first position
		public void add(Object data, int position){
		    if(head==null){
		    	head =new Node(data);
		    }
		    else{
		    	if(position<=0)//first position
		    		addFirst(data); 
		    	else if(position>=getSize()-1)//last position
		    		addLast(data);
		    	else{
		    		Node beforeReplacement = list.head;
			    	int count = 0; 
			    	while(count<=position-2)
			    	{
			    		beforeReplacement = beforeReplacement.nextItem;
			    		count++;
			    	}
			    	tail = beforeReplacement.nextItem;
			    	beforeReplacement.nextItem = new Node(data);
			    	beforeReplacement.nextItem.nextItem = tail;
		    	}
		    }
		}
		//Adds item in the first position
		public void delete(int position){
		    if(head==null){
		    	return;
		    }
		    else{
		    	if(position<=0)//first position
		    		deleteFirst(); 
		    	else if(position>=getSize()-1)//last position
		    		deleteLast();
		    	else{
		    		tail = head;
			    	int count = 0; 
			    	while(count<=position-2)
			    	{
			    		tail = tail.nextItem;
			    		count++;
			    	}
			    	Node nodeToReplace = tail.nextItem;
			    	tail.nextItem = nodeToReplace.nextItem;
		    	}
		    }
		}
	//Deletes a last item from the list	
	public void deleteLast() {
		if(head==null){
	    	return;
	    }
	    else{
	    	int size = getSize();
	    	if(size==1){
	    		head=tail = null;
	    	    return;
	    	}
	    	int count = 1;
	    	tail = head;
	    	while(tail.nextItem!=null && count<size-1){
	    		tail = tail.nextItem;
	    		count++;
	    	}
	    	tail = tail.nextItem = null;
	    	
	      }
	    }
	//Deletes a first item from the list	
	public void deleteFirst() {
		if(head==null){
	    	return;
	    }
	    else{
	    	   head = list.head.nextItem;
	    	}
		}
	//Returns a first item from the list
	public Object getFirst() {
		if(head==null){
	    	return null;
	    }
	    else{
	    	return head.data;
	      }
	}
	//Returns a last item from the list	
	public Object getLast() {
		if(head==null){
	    	return null;
	    }
	    else{
	    	tail = head;
	    	while(tail.nextItem!=null){
	    		tail = tail.nextItem;
	    	}
	    	return tail.data;
	      }
	 }
	//Clears the list	
	public void clear() {
		head = tail = null;
	 }
	
	//Returns only one instance of the LinkedList
	public static LinkedList<Object> getList(){
		if(list==null){
			list = new LinkedList<Object>();
		}
		return list;
	}
	//Returns item at a specified position 
	public Object get(int position){
	    if(head==null){
	    	return null;
	    }
	    else{
	    	Node beforeItem = list.head;
	    	if(position<=0)
	    		return getFirst();//first position
	    		
	    	else if(position>=getSize()-1)//last position
	    		return getLast();
	    	else{
		    	int count = 0; 
		    	while(count<=position-2)
		    	{
		    		beforeItem = beforeItem.nextItem;
		    		count++;
		    	}
	    	}
	    	return beforeItem.nextItem.data;
	    }
	}
	//Replaces item at a specified position	
	public void set(Object data,int position){
	    if(head==null){
	    	return;
	    }
	    else{
	    	Node beforeItem = list.head;
	    	if(position<=0)//first position
	    	{
	    		head.data = data; return;
	    	}
	    	else if(position>=getSize()-1)//last position
	    		{
	    		 tail.nextItem.data = data; return;
	    		}
	    	else{
		    	int count = 0; 
		    	while(count<=position-2)
		    	{
		    		beforeItem = beforeItem.nextItem;
		    		count++;
		    	}
	    	}
	    	beforeItem.nextItem.data = data;
	    }
	}
	//Returns the index of the first occurrence of the specified object 	
	public int firstIndexOf(Object data){
		int size = getSize();
	    if(head==null)
	    	return -1;
    	if(data==head.data)
    		return 0;
    	if(data==tail.nextItem.data)
    		return size-1;
	     Node nodeItem = head.nextItem;
	     if(data==nodeItem.data)
	    	 return 1;
	     int count = 1;
	     int index = -1;
	     while(nodeItem.nextItem!=null && index==-1){
	    	 nodeItem = nodeItem.nextItem;
	    	 count++;
	    	 index=data.equals(nodeItem.data)?count:-1;
	    	 
	     }
	    return index; 
	     
	}
	//Returns the index of the last occurrence of the specified object
	public int lastIndexOf(Object data){
		int size = getSize();
	    if(head==null)
	    	return -1;
    	if(data==tail.nextItem.data)
    		return size-1;
	     Node nodeItem = head.nextItem;
	     int count = 1;
	     int index = -1;
	     while(nodeItem.nextItem!=null){
	    	 nodeItem = nodeItem.nextItem;
	    	 count++;
	    	 index=data==nodeItem.data?count:-1;
	     }
	    return index; 
	     
	}
	//Returns list size
	public int getSize(){
		if(head==null)
			return 0;
		Node current = head;
		int size = 0;size++;
		while(current.nextItem!=null){
			current = current.nextItem;
			size++;
		}
		return size;
	}
	//Return string representation of the list
	public String toString(){
		if(head==null)
			return "[Empty list]";
		String string ="[";
		Node current = head;
		string+=current.data +", ";
		if(current.nextItem==null)
			return "["+current.data+"]";
		while(current.nextItem!=null){
			current = current.nextItem;
			if(current.nextItem!=null)
				string+=current.data +", ";
			else
				 string+=current.data +"]";
			
		}
	  return string;
	}
}
