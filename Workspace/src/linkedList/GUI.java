package linkedList;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class GUI extends JFrame implements ActionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel inputPanel;
	private JTextField inputArea;
	private static GUI gui = null;
	private static LinkedList<Object> list = null;
	private TextArea printList = new TextArea();
	private JPanel belowPanel;
    private GUI(LinkedList<Object> list) {
    	inputPanel = new JPanel();
    	printList = new TextArea(list+"");
    	belowPanel = new JPanel();
    	this.add(inputPanel);
    	this.add(printList);
    	this.add(belowPanel);
    	GUI.list = list;
    	apendEmptySpace();
    	drawGui();
    	
	}
    public static GUI getGUI(LinkedList<Object> list){
    	if(gui==null){
    		gui = new GUI(list);
    	}
    	return gui;
    }
    private void setBackgroundColor(Component comp, Color color){
    	comp.setBackground(color);
    }
	private void drawGui(){
    	this.setTitle("Linked List");
    	this.setLayout(new GridLayout(3,1,0,0));
    	inputPanel.setBackground(Color.BLACK);
    	inputPanel.setLayout(new GridLayout(2,1,0,0));
    	JLabel enterLabel = new JLabel("Enter item");
    	enterLabel.setForeground(Color.white);
    	enterLabel.setFont(new Font("Serif", Font.BOLD, 15));
    	inputArea = new JTextField(16);
    	JPanel textPanel = new JPanel();
    	textPanel.add(enterLabel);
    	textPanel.add(inputArea);
    	textPanel.setBackground(Color.BLACK);
    	JLabel hint = new JLabel("Hint: hover on a button to know what it does!");
    	hint.setFont(new Font("Serif", Font.BOLD, 15));
    	hint.setForeground(Color.RED);
    	textPanel.add(hint);
    	JPanel buttonsPanel = new JPanel();
    	buttonsPanel.setLayout(new GridLayout(2,10,10,5));
    	buttonsPanel.setBackground(Color.BLACK);
    	JButton addFirstItem = new JButton("Add First");
    	setBackgroundColor(addFirstItem, Color.cyan);
    	addFirstItem.setToolTipText("This button Inserts the specified element at the beginning of this list");
    	JButton addLastItem = new JButton("Add Last");
    	setBackgroundColor(addLastItem, Color.cyan);
    	addLastItem.setToolTipText("This button Inserts the specified element at the end of this list");
    	JButton addItemAt = new JButton("Add At");
    	setBackgroundColor(addItemAt, Color.cyan);
    	addItemAt.setToolTipText("This button Inserts the specified element at the specified position of this list");
    	JButton deleteFirstItem = new JButton("Delete First");
    	setBackgroundColor(deleteFirstItem, Color.cyan);
    	deleteFirstItem.setToolTipText("This button removes the first element from this list.");
    	JButton deleteLastItem = new JButton("Delete Last");
    	setBackgroundColor(deleteLastItem, Color.cyan);
    	deleteLastItem.setToolTipText("This button removes the last element from this list.");
    	JButton deleteItemAt = new JButton("Delete At");
    	setBackgroundColor(deleteItemAt, Color.cyan);
    	deleteItemAt.setToolTipText("This button removes the element at a specified position  from this list.");
    	JButton getFirstItem = new JButton("Get First");
    	setBackgroundColor(getFirstItem, Color.cyan);
    	getFirstItem.setToolTipText("This method returns the first element in this list.");
    	JButton getLastItem = new JButton("Get Last");
    	setBackgroundColor(getLastItem, Color.cyan);
    	getLastItem.setToolTipText("This method returns the last element in this list.");
    	JButton getItemAt = new JButton("Get At");
    	setBackgroundColor(getItemAt, Color.cyan);
    	getItemAt.setToolTipText("This method returns the element at a specified position in this list.");
    	JButton setItemAt = new JButton("Set At");
    	setBackgroundColor(setItemAt, Color.cyan);
    	setItemAt.setToolTipText("This button replaces the element at the specified position in this list with the specified element.");
    	
    	buttonsPanel.add(addFirstItem);
    	buttonsPanel.add(addLastItem);
    	buttonsPanel.add(addItemAt);
    	buttonsPanel.add(deleteFirstItem);
    	buttonsPanel.add(deleteLastItem);
    	buttonsPanel.add(deleteItemAt);
    	buttonsPanel.add(getFirstItem);
    	buttonsPanel.add(getLastItem);
    	buttonsPanel.add(getItemAt);
    	buttonsPanel.add(setItemAt);
    	
    	inputPanel.add(textPanel);
    	inputPanel.add(buttonsPanel);
    	
    	
    	printList.setForeground(Color.red);
    	printList.setBackground(Color.white);
    	printList.setFont(new Font("Serif", Font.BOLD, 30));
        printList.setEditable(false);
        
    	
        JButton clearAll = new JButton("Clear");
        clearAll.setPreferredSize(new Dimension(150, 70));
        setBackgroundColor(clearAll, Color.cyan);
        belowPanel.add(clearAll);
        belowPanel.setBackground(Color.BLACK);
       
    	
    	
    	
       //Add ActionListener to the buttons
       addFirstItem.addActionListener(this);
       addLastItem.addActionListener(this);
       addItemAt.addActionListener(this);
       deleteFirstItem.addActionListener(this);
       deleteLastItem.addActionListener(this);
       deleteItemAt.addActionListener(this);
       getFirstItem.addActionListener(this);
       getLastItem.addActionListener(this);
       getItemAt.addActionListener(this);
       setItemAt.addActionListener(this);
       clearAll.addActionListener(this);
    
    }
	@Override
	public void actionPerformed(ActionEvent e) {
		String s = e.getActionCommand();
        if (s.equals("Add First")) {
        	if(checkInput()){
        		list.addFirst(getInput());
            	apendEmptySpace();
        	}
        } 
        else if (s.equals("Add Last")) { 
        	if(checkInput()){
        		list.addLast(getInput());
            	apendEmptySpace();
        	}
        } 
        else if (s.equals("Add At")) { 
        	 if(checkInput()){
        		 try {  
        			 int position = Integer.parseInt(JOptionPane.showInputDialog("Enter the position you want to insert your item in\n Input must be a number!"));
        			 list.add(getInput(),position);
                  apendEmptySpace();
        	        
        	      } catch (NumberFormatException eX) {  
        	    	  JOptionPane.showMessageDialog(null,"Input must be a number!");
        	      } 
         	}
        } 
        else if (s.equals("Delete First")) {
        	if(list.getSize()==0){
   			 JOptionPane.showMessageDialog(null,"Nothing to delete! Empty list!");
   			 return;
        	}
        	list.deleteFirst();
        	apendEmptySpace();
        }
        else if (s.equals("Delete Last")) {
        	if(list.getSize()==0){
   			 JOptionPane.showMessageDialog(null,"Nothing to delete! Empty list!");
   			 return;
        	}
        	list.deleteLast();
        	apendEmptySpace();
        } 
        else if (s.equals("Delete At")) {
        	try {  
        		int position = Integer.parseInt(JOptionPane.showInputDialog("Enter the position you want to delete your item in\n Input must be a number!"));
        		if(list.getSize()==0){
        			 JOptionPane.showMessageDialog(null,"Nothing to delete! Empty list!");
        			 return;
        		}
   			 	list.delete(position);
   			 	apendEmptySpace();
   	      } catch (NumberFormatException eX) {  
   	    	  JOptionPane.showMessageDialog(null,"Input must be a number!");
   	      } 
        	
        }
        else if (s.equals("Get First")) { 
        	if(list.getSize()==0){
   			 JOptionPane.showMessageDialog(null,"Nothing to return! Empty list!");
   			 return;
        	}
        	JOptionPane.showMessageDialog(null,"First element in a list is: "+list.getFirst());
        	apendEmptySpace();
        } 
        else if (s.equals("Get Last")) { 
        	if(list.getSize()==0){
   			 JOptionPane.showMessageDialog(null,"Nothing to return! Empty list!");
   			 return;
        	}
        	JOptionPane.showMessageDialog(null,"Last element in a list is: "+list.getLast());
        	apendEmptySpace();
        } 
        else if (s.equals("Get At")) {
			 	try {  
	        		int position = Integer.parseInt(JOptionPane.showInputDialog("Enter the position you want to return your item from\n Input must be a number!"));
	        		if(list.getSize()==0){
	        			 JOptionPane.showMessageDialog(null,"Nothing to return! Empty list!");
	        			 return;
	        		}
	        		JOptionPane.showMessageDialog(null,"Returned element in a list is: "+list.get(position));
				 	apendEmptySpace();
	   	      } catch (NumberFormatException eX) {  
	   	    	  JOptionPane.showMessageDialog(null,"Input must be a number!");
	   	      } 
        } 
        else if (s.equals("Set At")) { 
        	try {  
        		int position = Integer.parseInt(JOptionPane.showInputDialog("Enter the position you want to replace\n Input must be a number!"));
        		if(list.getSize()==0){
        			 JOptionPane.showMessageDialog(null,"Nothing to replace! Empty list!");
        			 return;
        		}
        		if(checkInput()){
        			list.set(getInput(), position);
    			 	apendEmptySpace();
        		}
   	      } catch (NumberFormatException eX) {  
   	    	  JOptionPane.showMessageDialog(null,"Input must be a number!");
   	      } 
        }
        
        if (s.equals("Clear")) {
        	if(list.getSize()==0){
   			 JOptionPane.showMessageDialog(null,"Nothing to clear! Empty list!");
   			 return;
   		}
        	list.clear(); inputArea.setText("");
        	apendEmptySpace();
        } 
		
	}
	private boolean checkInput() {
		if(inputArea.getText().isEmpty()){
			JOptionPane.showMessageDialog(null, "Empy input!");
			return false;
		}
		return true;
	}
	private Object getInput() {
		return inputArea.getText();
	}
	private void apendEmptySpace(){
		printList.setText(list+"");
	}
}
