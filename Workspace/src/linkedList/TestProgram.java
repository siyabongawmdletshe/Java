package linkedList;

import javax.swing.JFrame;

public class TestProgram {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		LinkedList<Object> list = LinkedList.getList();
		GUI gui = GUI.getGUI(list);
		 gui.setSize(800, 700); 
		 gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 gui.setResizable(false);
		 gui.setVisible(true);
	}

}
