/**
 * 
 */
package myphonebook;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;
/**
 * @author Siyabonga Wonderboy Mdletshe
 * 9 October 2019
 *
 */
public class Phonebook {

	/**
	 * @param args
	 */
	
	//array to act as a database
	private static Map<Long, String> phonebook = new HashMap<Long, String>() ;
	private static Scanner readUserInput=new Scanner(System.in);
	private static int operation = -1;
    
	public static void main(String[] args) {

		try {
			//Repeat operation until user exit program
			do{
				
				System.out.println();
				//select operation
				System.out.println("Please select operaton:\n1 - Add Number\n2 - Search Number" +
						"\n3 - Delete Number\n4 - Display Listt\n5 - Delete All\n6 - Exit");
				operation = readUserInput.nextInt();
				String name ="";
				long number =0;
				if(operation<4){
					System.out.print("Enter a name: ");name=readUserInput.next();
					System.out.print("Enter a number: ");
					number=readUserInput.nextLong();
				}
				switch (operation) {
				case 1:	
				        if(NumberIsValid(number)){
				        	if(AddNumber(number,name)){
					        	System.out.println("The record was successfully added to the phonebook!");
					        }
					        else{
					        	System.out.println("The record already exits!");
					        }
				        }
				        else{
				        	System.out.println("Please make sure your number only contains 10 digits (i.e. 0724567893 ) ");
				        	operation = 0;
				        }
				        break;
				case 2:	
					 	if(NumberIsValid(number)){
					 		if(SearchNumber(number,name)){
								System.out.println("The record exits on the phonebook!");	
							}
							else{
								System.out.println("The record does not exit on the phonebook!");
							}
					 	}
					 	else{
				        	System.out.println("Please make sure your number only contains 10 digits (i.e. 0724567893 ) ");
				        	operation = 0;
				        }
						break;
				case 3: 
					 	if(NumberIsValid(number)){
					 		 if(DeleteNumber(number,name)){
						        	System.out.println("The record was successfully deleted from the phonebook!");
						        }
						        else{
						        	System.out.println("The record does not exit on the phonebook!");
						        }
					 	}
					 	else{
				        	System.out.println("Please make sure your number only contains 10 digits (i.e. 0724567893 ) ");
				        	operation = 0;
				        }
				        break;
				case 4: 
				        DisplayList(); break;
				case 5: 
			         if(phonebook.size()==0){
			        	 System.out.println("Nothing to delete! The phonebook is empty!");
			        	 operation = 0;
			         }
			         else{
			        	 if(DeleteAll()==0){
				        	 System.out.println("The phonebook was successfully deleted!");
				         }
			         }
			         break;
				case 6: System.out.println("Goodbye....");
		        Exit(); break;
				default:break;
				}
			}
			while(operation==0);
		} catch (InputMismatchException e) {
			System.out.println("Invalid number!!!! Please make sure your number only contains 10 digits (i.e. 0724567893 )");
			System.out.println("Goodbye....");
			operation=0;
		}
	}
	private static boolean NumberIsValid(long numberToAdd) {
		if(String.valueOf(numberToAdd).length()==10){
			return true;
		}
		return false;
	}
	private static int DeleteAll() {
		if(phonebook.size()>0){
			phonebook.clear();
		}
		operation = 0;
		return phonebook.size();
	}
	private static void DisplayList() {
		System.out.println("Number"+"\t\t\t"+"Name");
		for(Map.Entry<Long, String>phonebookList: phonebook.entrySet()){
			System.out.println(phonebookList.getKey() +"\t\t"+phonebookList.getValue());
	    }
		operation = 0;
		System.out.println();
	}
	private static void Exit() {
		readUserInput.close();
		operation = -1;
		phonebook = null;
	}
	private static boolean DeleteNumber(long numberToDelete, String name) {
		operation = 0;
		//delete only if it exits
		if(SearchNumber(numberToDelete,name)){
			phonebook.remove(numberToDelete);
			return true;
		}
		return false;
	}
    private static boolean SearchNumber(long numberToFind, String name) {
		operation = 0;
		for(Map.Entry<Long, String>phonebookSearch: phonebook.entrySet()){
			if(phonebookSearch.getKey()==numberToFind && phonebookSearch.getValue().equalsIgnoreCase(name)){
				return true;
			}
		}
		return false;
	}
	private static boolean AddNumber(long numberToAdd, String name) {
		operation = 0;
		//search if the number already exits. If it exits don't add it to the phonebook.
		if(SearchNumber(numberToAdd,name)){
			return false;
		}
		phonebook.put(numberToAdd, name);
		return true;
	}
}