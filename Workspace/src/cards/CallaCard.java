/**
 * 
 */
package cards;
import java.util.Scanner;
/**
 * @author siyabongawmdletshe
 *
 */
public class CallaCard {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner reader = new Scanner(System.in);
		
		try {
			int suit = 0;
			int value = 0;
			int turns =0;
			boolean winnner =false;
			Deck deck = new Deck();
			do{
				do{
					System.out.print("Please pick a card ");
					suit = reader.nextInt();
					value = reader.nextInt();
				}
				while((suit<0 || suit>4) || (value<0 || value>13));
				   deck.shuffle();
				   Card card = null;
				   while(!winnner){
					   card = deck.dealCard();
					   int suitCard = card.getSuit();
					   int valueCard = card.getValue();
					   System.out.println(turns+" : "+card.toString());
					   if(suit==suitCard && value==valueCard){
						   winnner=true;
						   System.out.println("Winner is "+turns);
					   }
					   if(turns==0){
						   turns++;
					   }
					   else{
						   turns =0;
					   }
				   }
				   System.out.println("");
				   winnner=false;
			}
			while(true);
			
		} catch (Exception e) {
			System.out.print(e);
		}
		
		
		
		

	}

}
