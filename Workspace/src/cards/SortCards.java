/**
 * 
 */
package cards;

/**
 * @author siyabongawmdletshe
 *
 */
public class SortCards {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Card a = new OrderedCard(3 ,0);
		Card b = new OrderedCard (4,0);

		OrderedCard aa = (OrderedCard)a;
		OrderedCard bb = (OrderedCard)b;
		
		if(a.getSuit()==b.getSuit() && a.getValue()==b.getValue()){
			System.out.println("Same cards!");
			return;
		}
		if(aa.precedes(bb)){
			System.out.println(a.getValueAsString()+" of "+a.getSuitAsString() +" comes before "
		    +b.getValueAsString()+" of "+b.getSuitAsString());
			return;
		}
		if(aa.follows(bb)){
			System.out.println(a.getValueAsString()+" of "+a.getSuitAsString() +" comes after "
		    +b.getValueAsString()+" of "+b.getSuitAsString());
		}

	}

}
