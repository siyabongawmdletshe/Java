package cards;

public class OrderedCard extends Card implements Ordered {

	public OrderedCard(int value, int suit){
		super(value,suit);
	}
	@Override
	public boolean precedes(Object other) {
		if(other instanceof Card){
			Card o = (Card)other;
			if(this.getSuit()==o.getSuit()){
				if(this.getValue()<o.getValue()){
					return true;
				}
			}
			else if(this.getSuit()<o.getSuit()){
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean follows(Object other) {
		if(other instanceof Card){
			Card o = (Card)other;
			if(this.getSuit()==o.getSuit()){
				if(this.getValue()>o.getValue()){
					return true;
				}
			}
			else if(this.getSuit()>o.getSuit()){
				return true;
			}
		}
		return false;
	}

}
