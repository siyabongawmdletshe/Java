package cards;

public interface Ordered {
	/** return true if this object comes before other */
	public boolean precedes(Object other) ;
	/** return true if this object comes after other */
	public boolean follows(Object other) ;
}
