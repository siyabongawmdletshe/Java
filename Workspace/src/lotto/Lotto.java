/**
 * 
 */
package lotto;
import java.util.Scanner;
/**
 * @author siyabongawmdletshe
 *
 */
public class Lotto {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner reader = new Scanner(System.in);
		
		try {
			int random = 10+(int)(Math.random()*90);
			int userInput = 0;
			
			do{
				do{
					System.out.println("Please enter a 2 digits number:");
					userInput = reader.nextInt();
				}
				while(userInput<10 || userInput>99);
				   System.out.println("Lotto number: " +random);
					System.out.println("Your guess: " +userInput);
					
					if(random==userInput){
						System.out.println("You got it right! You win $10,000");
					}
					else{
						int firstDigit =userInput/10;
						int secondDigit =userInput%10;
						int firstLotoDigit =random/10;
						int secondLotoDigit =random%10;
						if(firstDigit==secondLotoDigit && secondDigit==firstLotoDigit){
							System.out.println("All digits match! You win $3,000");
						}
						else if((firstDigit==firstLotoDigit && secondDigit!=secondLotoDigit) 
								||(firstDigit!=firstLotoDigit && secondDigit==secondLotoDigit)
								|| (firstDigit==secondLotoDigit && secondDigit!=firstLotoDigit)
								||(firstDigit!=secondLotoDigit && secondDigit==firstLotoDigit)){
							     System.out.println("One number is a match! You win $1,000");
							
						}
						else{
							System.out.println("Sorry! Your guess number did not match!");
						}
					}
					System.out.println("");
			}
			while(true);
			
		} catch (Exception e) {
			System.out.print(e);
		}
		
		
		
		

	}

}
