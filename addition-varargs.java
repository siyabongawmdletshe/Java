import java.io.*;
import java.lang.reflect.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

/**
You are given a class Solution and its main method in the editor.
Your task is to create the class Add and the required methods so that 
the code prints the sum of the numbers passed to the function add.
***/

class Add {

    public void add(int ...list) 
    {
            int lSize = list.length;
            String sBuilder ="";
            int sum = 0;  
            for (int i=0; i<lSize; i++){
                sum+=list[i];
              if(i<lSize-1){
               sBuilder+=list[i] +"+";
              }
              else{
                  sBuilder+=list[i] +"=";
              }
            } 
            
            System.out.println(sBuilder+""+sum); 
        } 
}

public class Solution {

    public static void main(String[] args) {
       try{
			BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
			int n1=Integer.parseInt(br.readLine());
			int n2=Integer.parseInt(br.readLine());
			int n3=Integer.parseInt(br.readLine());
			int n4=Integer.parseInt(br.readLine());
			int n5=Integer.parseInt(br.readLine());
			int n6=Integer.parseInt(br.readLine());
			Add ob=new Add();
			ob.add(n1,n2);
			ob.add(n1,n2,n3);
			ob.add(n1,n2,n3,n4,n5);	
			ob.add(n1,n2,n3,n4,n5,n6);
			Method[] methods=Add.class.getDeclaredMethods();
			Set<String> set=new HashSet<>();
			boolean overload=false;
			for(int i=0;i<methods.length;i++)
			{
				if(set.contains(methods[i].getName()))
				{
					overload=true;
					break;
				}
				set.add(methods[i].getName());
				
			}
			if(overload)
			{
				throw new Exception("Overloading not allowed");
			}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}

}

