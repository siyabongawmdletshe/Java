import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

  /**
  Given five positive integers, find the minimum and maximum values that can be calculated by summing exactly four of the five integers. 
  Then print the respective minimum and maximum values as a single line of two space-separated long integers.
  **/
    public static long getMaxValue(long[] numbers){
    long maxValue = numbers[0];
    for(int i=1;i < numbers.length;i++){
        if(numbers[i] > maxValue){
        maxValue = numbers[i];
        }
    }
    return maxValue;
    }
    public static long getMinValue(long[] numbers){
    long minValue = numbers[0];
    for(int i=1;i<numbers.length;i++){
        if(numbers[i] < minValue){
        minValue = numbers[i];
        }
    }
    return minValue;
    }
   
    static void miniMaxSum(int[] arr) {
    long sum =0;
    long[] arrSums = new long[5];
    for (int i = 0; i < 5; i++) {
       for (int j = 0; j < 5; j++){
        if(i!=j){
         sum+=arr[j];
        }
        }
        arrSums[i] = sum;
        sum = 0;     
    }
    System.out.print(getMinValue(arrSums) +" " + getMaxValue(arrSums));
   }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[] arr = new int[5];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < 5; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        miniMaxSum(arr);

        scanner.close();
    }
}
