import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

/**
We define the following:
A subarray of an n-element array is an array composed from a contiguous block of the original array's elements. 
For example, if array =[1,2,3], then the subarrays are [1],[2],[3],[1,2],[2,3], and [1,2,3]. 
Something like [1,3] would not be a subarray as it's not a contiguous subsection of the original array.
The sum of an array is the total sum of its elements.
An array's sum is negative if the total sum of its elements is negative.
An array's sum is positive if the total sum of its elements is positive.
Given an array of  integers, find and print its number of negative subarrays on a new line.
***/
    private static final Scanner scanner = new Scanner(System.in);
    public static void main(String[] args ) throws IOException {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
           
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = Integer.parseInt(bufferedReader.readLine().trim());
        String[] sItems = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");
        int []arr = new int [n];
        for (int i = 0; i < n; i++) {
            int sItem = Integer.parseInt(sItems[i]);
            arr[i] = sItem;
        }
           
            int size = arr.length;
           
             List<Integer> listOfSums = new ArrayList<Integer>();
            int sum = 0; 
            for (int i = 0; i < size; i++)
            { 
                if (arr[i]<0) {
                    listOfSums.add(arr[i]);
                }
                for (int j = i+1; j <size; j++)
                {
                    if (arr[i] + arr[j] < 0 && j-i==1)
                    {
                        listOfSums.add(arr[i] + arr[j]);
                    }
                }
            }
            if (size>2) {
               
                for (int i = 0; i < size; i++)
                {
                    if (i+1<size) {
                        sum += arr[i] + arr[i + 1];
                    }
                    for (int j = i+2; j < size; j++)
                    {
                        sum += arr[j];
                        if (sum < 0)
                        {
                            listOfSums.add(sum);
                        }
                       
                    }
                    sum = 0;
                }
            }
        bufferedWriter.write(String.valueOf(listOfSums.size()));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}

