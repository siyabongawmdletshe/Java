import java.util.*; 
public class Main {
  public static void main(String[] args) {
  // Write your code here
   int [] values = new int[5];
   int sum = 0;
   int length = values.length;
   
   Random rand = new Random();
   for(int i =0; i<length; i++){
     
     int r = rand.nextInt(5)+1;
     values[i] = r;
   }
   
   System.out.println("values of an array are: ");
   for(int i =0; i<length; i++){
    System.out.print(values[i]+" ");
   }
   
 System.out.println();
   for(int i =0; i<length; i++){
     sum+=values[i];
   }
   System.out.println("sum values of an array is: "+sum);
 }
}