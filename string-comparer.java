import java.util.Scanner;

public class Solution {
/**
Given a string, s ,and an integer, k, 
complete the function so that it finds the lexicographically smallest and largest substrings of length .
***/
   public static int stringCompare(String str1, String str2) 
    { 
  
        int l1 = str1.length(); 
        int l2 = str2.length(); 
        int lmin = Math.min(l1, l2); 
  
        for (int i = 0; i < lmin; i++) { 
            int str1_ch = (int)str1.charAt(i); 
            int str2_ch = (int)str2.charAt(i); 
  
            if (str1_ch != str2_ch) { 
                return str1_ch - str2_ch; 
            } 
        } 
  
        // Edge case for strings like 
        // String 1="Welcome and String 2="welcometojava" 
        if (l1 != l2) { 
            return l1 - l2; 
        } 
  
        // If none of the above conditions is true, 
        // it implies both the strings are equal 
        else { 
            return 0; 
        } 
    }
    public static String getSmallestAndLargest(String s, int k) {
        String smallest = "";
        String largest = "";
        
        
        int sizez = s.length();
        String subs ="";
        String sub []=new String[sizez - (k - 1)]; 
        
        for (int i = 0; i<sizez - (k - 1); i++)
            {
                subs = s.substring(i, (i+k));
                sub[i]=subs;
            }
    
        smallest = sub[0];
        for (int i = 0; i<sub.length; i++)
        {
            if(stringCompare(sub[i], smallest)<0){
            smallest = sub[i];
            }
        }
        largest= sub[0];
        for (int i = 0; i<sub.length; i++)
        {
            if(stringCompare(sub[i], largest)>0){
            largest = sub[i];
            }
        } 

        return smallest + "\n" + largest;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.next();
        int k = scan.nextInt();
        scan.close();
      
        System.out.println(getSmallestAndLargest(s, k));
    }
}