import java.util.*;
class Solution{
    /***
	A string containing only parentheses is balanced if the following is true: 1. 
	if it is an empty string 2. if A and B are correct, AB is correct, 3. 
	if A is correct, (A) and {A} and [A] are also correct.
	Examples of some correctly balanced strings are: "{}()", "[{()}]", "({()})"
	Examples of some unbalanced strings are: "{}(", "({)}", "[[", "}{" etc.
	Given a string, determine if it is balanced or not.	
	****/
    public static boolean ParentesisBalanced(String input){
            if (input.isEmpty()){
                return true;
            }
            Stack<Character> stack = new Stack<Character>();
            int stringL = input.length();
            for(int i=0; i<stringL; i++){
                char currentCurrent = input.charAt(i);
                if(currentCurrent=='{' || currentCurrent=='[' || currentCurrent=='('){
                   stack.push(currentCurrent);
                }
                else if(currentCurrent=='}' || currentCurrent==']' || currentCurrent==')'){
                   if (stack.isEmpty()){
                    return false;
                   }
                    
                    char lastCharacter = stack.peek();
                    if((currentCurrent =='}' && lastCharacter=='{') || (currentCurrent==']' && lastCharacter=='[') ||                                   (currentCurrent==')' && lastCharacter=='(')){
                        stack.pop();
                    }
                    else{
                        return false;
                    }
             
                }

            }
            return stack.isEmpty();
    }
    
    public static void main(String []argh)
    {
        Scanner sc = new Scanner(System.in);
        try{
            while (sc.hasNext()){
                String input=sc.next();
                System.out.println(ParentesisBalanced(input));
            } 
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }     
}
