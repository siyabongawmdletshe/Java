import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
   /**
   You are given  lines. In each line there are zero or more integers. 
   You need to answer a few queries where you need to tell the number located in y position of x line.
   ***/
    public static void main(String[] args) throws IOException {
      BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
      BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int linesNumber = Integer.parseInt(bufferedReader.readLine().trim());
        List<int []> lines = new ArrayList<int []>();
        List<int []> queries = new ArrayList<int []>();
        for (int i = 0; i < linesNumber; i++) {
             String[] sItems = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");
             int eachlineN = Integer.parseInt(sItems[0]);
             int []arr = new int [eachlineN];
             for (int j = 0; j < eachlineN ; j++) {
             arr[j] = Integer.parseInt(sItems[j+1]);
             }
            lines.add(arr);
        }
        //Read Queries
        int queriesN = Integer.parseInt(bufferedReader.readLine().trim());
        //System.out.println(queriesN);
        for(int i =0; i<queriesN; i++){
         String[] xy = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");
         int []ar = new int [2]; 
         for (int j = 0; j < 2 ; j++) {
             ar[j] = Integer.parseInt(xy[j]);
         }
          queries.add(ar);
        }
        
 
      //print a number located in line x and position index y
       for (int [] intArr : queries) {
            int lSize = lines.get(intArr[0]-1).length;
            int [] line = lines.get(intArr[0]-1);
            
            if(line!=null && lSize>0){
             if((intArr[1]-1)<lSize){
               bufferedWriter.write(String.valueOf(line[intArr[1]-1]));
               bufferedWriter.newLine();
             }
             else{
               bufferedWriter.write(String.valueOf("ERROR!"));
               bufferedWriter.newLine();
             }
 
            }
            else{
                 bufferedWriter.write(String.valueOf("ERROR!"));
                 bufferedWriter.newLine();
            }

        }
    bufferedReader.close();
    bufferedWriter.close();
 }

}