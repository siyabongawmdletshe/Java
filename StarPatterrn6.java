import java.util.*; 
public class Main {
  public static void main(String[] args) {
   String star ="*";
   int rows  = 8;
   int cols = 1;
   System.out.println("Star(*) Pattern 6");
   for(int r =1; r<=rows; r++){
     for(int c =0; c<cols; c++){
       System.out.print(star);
     }
     System.out.print(" ");
     for(int c =0; c<cols; c++){
       System.out.print(star);
     }
     System.out.println();
     cols++;
   }
}
}
/* output

* *
** **
*** ***
**** ****
***** *****
****** ******
******* *******
******** *********

*/