import java.util.*; 
public class Main {
  public static void main(String[] args) {
   String star ="*";
   int rows  = 8;
   int cols = 1;
   int spaces =0;
   System.out.println("Star(*) Pattern 5");
   for(int r =1; r<=rows; r++){
     spaces = rows - r;
	   for(int s1 =0; s1<spaces; s1++){
       System.out.print(" ");
     }
     for(int c =0; c<cols; c++){
       System.out.print(star);
     }
     System.out.println();
     cols++;
   }
}}
/* output

       *
      **
     ***
    ****
   *****
  ******
 *******
********

*/