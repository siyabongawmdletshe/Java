import java.util.*; 
public class Main {
  public static void main(String[] args) {
  // Write your code here
   int [] values = new int[5];
   int sum = 0;
   int length = values.length;

   Random rand = new Random();
   for(int i =0; i<length; i++){
     
     int r = rand.nextInt(100)+1;
     values[i] = r;
   }
   
   System.out.println("values of an array are: ");
   for(int i =0; i<length; i++){
    System.out.print(values[i]+" ");
   }
   
   int small = smallest(values);
   System.out.println("the smallest number among array numbers: "+small);
  
 }
 private static int smallest(int [] values){
   if(values.length<0){
     return -1;
   }
   Arrays.sort(values);
   return values[0];
 }
}