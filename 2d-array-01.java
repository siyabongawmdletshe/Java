import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

/**
You are given a 6x6 2D array. An hourglass in an array is a portion shaped like this:

a b c
  d
e f g

For example, if we create an hourglass using the number 1 within an array full of zeros, it may look like this:

1 1 1 0 0 0
0 1 0 0 0 0
1 1 1 0 0 0
0 0 0 0 0 0
0 0 0 0 0 0
0 0 0 0 0 0

The sum of an hourglass is the sum of all the numbers within it. 
The sum for the hourglasses above are 7, 4, and 2, respectively.
In this problem you have to print the largest sum among all the hourglasses in the array.
***/

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[][] arr = new int[6][6];

        for (int i = 0; i < 6; i++) {
            String[] arrRowItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int j = 0; j < 6; j++) {
                int arrItem = Integer.parseInt(arrRowItems[j]);
                arr[i][j] = arrItem;
            }
        }
        int countRows = 0;
        int sum = 0;
        List<Integer> listOfSums = new ArrayList<>(); 
        while (countRows<4) {
            for (int j = 0; j <= 3; j++)
            {
             sum += arr[countRows][j] + arr[countRows][j + 1] + arr[countRows] [j+ 2];
             sum += arr[countRows + 1][j + 1];
             sum += arr[countRows+2][j] + arr[countRows+2][j + 1] + arr[countRows+2] [j + 2];
             listOfSums.add(sum);     
             sum = 0;
             }
            countRows++;
        }
        Collections.sort(listOfSums); 
        System.out.println(listOfSums.get(listOfSums.size() - 1));
        scanner.close();
    }
}
